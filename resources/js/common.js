function menuFun() {
    $selector = $('.menu-list > li> a');
    $selector.on('click', function() {
        $(this).parent().toggleClass('active').siblings().removeClass('active');
    });

}
menuFun();

function contentSize() {
    var $selector = $('.top_menu .toggle'),
        $gnb = $('.gnb'),
        $dash = $('.dashboard');
    $selector.on('click', function() {
        toggleFn([$selector, $gnb, $dash], 'active');
    });
}
contentSize();

function menuSlide() {
    var $selector = $('.menu-btn'),
        $target = $('.favorites');
    $selector.on('click', function() {
        toggleFn([$target], 'active');
    });
}
menuSlide();

function userMenu() {
    var $selector = $('.user-util'),
        $target = $('.link-list');
    $selector.on('click', function() {
        toggleFn([$selector, $target], 'active');
    });
}
userMenu();

function selectFn() {
    var $select = $(".select"),
        $target;
    $select.children().not("select").remove();
    $(document).off("click").on("click", function(e) {
        var $target = $(e.target);
        if (!$target.is(".select, .selected,.select-list")) {
            $select.removeClass("on");
        }
    });
    $select.each(function() {
        var $this = $(this),
            $orgSelect = $this.children("select"),
            $orgData = $orgSelect.attr("name"),
            $orgOption = $orgSelect.children("option"),
            $selected = $("<div class='selected'></div>"),
            $selectList = $("<ul class='select-list'></ul>");
        $this.append($selected, $selectList);
        $orgOption.each(function(idx) {
            var $selectItem = $("<li class='select-item'></li>");
            if ($(this).filter(":selected").length != 0) {
                $selectItem = $("<li class='select-item on'></li>");
                $selected.text($(this).text());
            }
            $selectItem.text($(this).text());
            $selectList.append($selectItem).attr("id", $orgData);
        });
    });

    $(".select").off().on("click", function() {
        var $this = $(this),
            $list = $this.children(".select-list"),
            $item = $list.children(".select-item");
        $text = $this.children(".selected");
        if ($this.offset().top > $(window).height() / 2) {
            $this.addClass("revert");
        }
        $select.removeClass("on");

        $this.toggleClass("on");
        $list.on("mouseleave", function() {
            $this.removeClass("on");
        });
        $item.off().on("click", function(e) {
            var $this = $(this),
                $idx = $this.index(),
                $option = $this.parent(".select-list").siblings("select").children("option");
            $text.addClass('active');
            $option.eq($idx).prop("selected", true).siblings($option).prop("selected", false);
            $text.text($this.text());
            $this.addClass("on").siblings().removeClass("on");
            $option.parent().trigger("change");
            setTimeout(function() {
                $select.removeClass("on");
            }, 100);
        });
    });
}
selectFn();

$('.datepick').each(function() {
    $(this).datepicker();
});

function searchToggle() {
    var $selector = $('.toggle-box .toggle'),
        $target = $('.toggle-box');
    $selector.on('click', function() {
        toggleFn([$target, $selector], 'active');
    });
}
searchToggle();

function tabFn() {
    var $tab1 = $('.round-tab button'),
        $tab1_cont = $('.tab_cont'),
        $tab2 = $('.tab-list button'),
        $tab2_cont = $('.tab_cont.active .tab-list-cont');
    $tab1.on('click', function() {
        $tab1.removeClass('active');
        $(this).addClass('active');
        $tab1_cont.removeClass('active');
        $tab1_cont.eq($(this).index()).addClass('active');
    });
    $tab2.on('click', function() {
        $idx = $(this).index();
        $(this).addClass('active').siblings().removeClass('active');
        console.log($tab2_cont);
        $tab2_cont.eq($idx).addClass('active').siblings().removeClass('active');
    });
}
tabFn();

function fileFn() {
    var $fileWrap = $(".file-wrap"),
        $fileName = $fileWrap.children(".txt");
    $fileWrap.children("input.file").on("change", function(e) {
        $fileName.val($(this).val());
    });
}
fileFn();

function toggleFn($elem, $class) {
    $.each($elem, function() {
        var $this = $(this);
        $this.toggleClass($class);
    });
}

function tableToggle() {
    var $selector = $('.scoll-table h2 .toggle'),
        $target = $('table.directory');
    $selector.on('click', function() {
        toggleFn([$target, $selector], 'active');
    });
}
tableToggle();
